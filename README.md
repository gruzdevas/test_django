Для запуска необходимо установить необходимые библиотеки командой 
pip install requirements.txt (глобально или в виртуальном окружении)
После этого нужно прописать следующие команды для миграции базы данных
py manage.py makemigrations
py manage.py migrate
И для запуска сервера написать
py manage.py runserver

