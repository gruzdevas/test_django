from django.contrib import admin
from django import forms
from main.models import Urls
from django.core.management import call_command

#Указываем какие поля убрать из формы
class UrlForm(forms.ModelForm):
    class Meta:
        model = Urls
        exclude = ['tried', 'creation_time', 'succeeded', 'headers', 'title', 'encoding']
        
@admin.register(Urls)
class UrlAdmin(admin.ModelAdmin):
    form = UrlForm
    #При сохранении модели вызовется команда джанго
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        call_command('parser')
# Register your models here.
