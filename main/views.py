from django.shortcuts import render
from main.models import Urls
from datetime import datetime
import pytz

# View для главной страницы
def index(request):
    """View function for home page of site."""
    urls = Urls.objects.all().order_by('-creation_time')
    messages = ''
    logs = ''
    for url in urls:
        messages += url.creation_time.astimezone(pytz.timezone('Asia/Krasnoyarsk')).strftime("%d.%m.%Y %H:%M:%S") + ": " + str(url)
        if (url.title or url.encoding or url.headers):
            logs += str(url) + " - "
            if url.title:
                logs += "Заголовок: " + url.title + ", "
            if url.encoding:
                logs += "Кодировка: " + url.encoding + ", "
            if url.headers:
                logs += "H1: " + url.headers
        else:
            logs += str(url)
        if url.succeeded:
             messages += ' Успех'
        elif url.tried:
             messages += ' Провал'
        else:
             messages += ' В обработке'
        messages += '\n'
        logs += '\n'


    context = {'urls' : urls, 'messages' : messages, 'logs' : logs}
    return render(request, 'index.html', context)