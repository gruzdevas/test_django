import urllib.request
from main.models import Urls
import threading
from django.core.management.base import BaseCommand, CommandError
import re
from bs4 import BeautifulSoup as BS

class Command(BaseCommand):
    
    # Запрос к url, возвращает html
    def get_html(self, url):
        try:
            response = urllib.request.urlopen(url)
        except ValueError:
            response = ""
        return response

# Считать кодировку из html
    def get_encoding(self, soup):
        meta = soup.findAll('meta')
        encoding = ""
        for tag in meta:
            encoding = tag.get('charset')
            if encoding == None:
                encoding = tag.get('content-type')
                if encoding == None:
                    content = tag.get('content')
                    match = re.search('charset=(.*)', content)
                    if match:
                        encoding = match.group(1)
                        return encoding
                else:
                    return encoding
            else:
                return encoding

    # Получаем html, записываем необходимые данные в объект
    def set_data(self, url_id):
        url = Urls.objects.get(id = url_id)
        html = self.get_html(url.url)
        url.tried = True
        if (html != ""):
            url.succeeded = True
            soup = BS(html, features="html.parser")
            title = soup.title
            headers = soup.find('h1')
            encoding = self.get_encoding(soup)
            if title:
                url.title = title.string
            url.encoding = encoding
            if headers:
                url.headers = headers.string
        url.save()

    #Обработчик вызова команды
    def handle(self, *args, **options):
        for url in Urls.objects.filter(tried__exact = False).order_by('timeshift'):
            url.save()
            if (url.timeshift):
                time = int(url.timeshift.minute)*60+int(url.timeshift.second)
            else: 
                time = 1 # Если время не указано, то таймер 1 секунда, т.к. если ставить 0, функция не может найти объект
            #Создаем поток, в котором ставим таймер на выполнение парсинга
            threading.Thread(target=threading.Timer(time, self.set_data, args = [url.id]).start(), args = (url.id),daemon=True).start()
