from django.db import models
from datetime import datetime
from django.utils import timezone

class Urls(models.Model):
    """Модель ссылки."""
    url = models.CharField(
        max_length=200, help_text='URL ресурса')
    timeshift = models.TimeField(null=True, blank=True, help_text='Через какое время обработать')
    tried = models.BooleanField(default=False)
    succeeded = models.BooleanField(default=False)
    creation_time = models.DateTimeField(default=datetime.now(), help_text='Через какое время обработать')
    title = models.CharField(max_length=50, null=True, blank=True)
    encoding = models.CharField(max_length=50, null=True, blank=True)
    headers = models.CharField(max_length=500, null=True, blank=True)
    def __str__(self):
        return self.url

# Create your models here.
