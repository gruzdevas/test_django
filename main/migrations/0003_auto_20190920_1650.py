# Generated by Django 2.2.5 on 2019-09-20 09:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20190920_1114'),
    ]

    operations = [
        migrations.AddField(
            model_name='urls',
            name='response',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='urls',
            name='tried',
            field=models.BooleanField(default=False),
        ),
    ]
